#ifndef _MDP
#define _MDP
	
	typedef struct {
		int array_int[1000];
		double fitness;
	} Individuo;
	
	void cruzar(Individuo *, Individuo *, Individuo *, Individuo *, int, int);
	void fitness(const double *, Individuo *, int, int);
	void mutar(Individuo *, int, int);
	double getDynamicMutationRate(int size);

	MPI_Datatype MPI_INDIVIDUO;
#endif
