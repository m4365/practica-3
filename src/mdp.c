#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../include/mdp.h"
#include "../include/io.h"

#define MUTATION_RATE 0.4
#define PRINT 0
#define NUM_GEN 5
#define NUM_IND 5

int aleatorio(int n) {
    unsigned int seed = (int) rand() % RAND_MAX;   // genera un valor para la semilla usando la función rand

    return ((int) (rand_r(&seed) % n));  // genera un numero aleatorio entre 0 y n-1 usando la función rand_r
}

float random_float(float min, float max)
{
	if (min == max) {
		return min;
	} else if(min < max) {
		unsigned int seed = (int) rand() % RAND_MAX;

		return (max - min) * ((float)rand_r(&seed) / RAND_MAX) + min;
	}

	return 0;
}

int find_element(int *array, int end, int element)
{
	int i=0;
	int found=0;
	
	// comprueba que un elemento no está incluido en el individuo (el cual no admite enteros repetidos)
	while((i < end) && ! found) {
		if(array[i] == element) {
			found = 1;
		}
		i++;
	}
	return found;
}

/**
 * @brief Get the local population object
 * Esta funcion se encarga de recibir la poblacion local de el proceso root
 * para calcular GA en cada isla.
 * 
 * @param poblacion 
 * @param tam_poblacion 
 * @param rank 
 */
void get_local_population(
	Individuo **poblacion,
	int tam_poblacion,
	int rank
) {
	Individuo buffer[tam_poblacion];

	MPI_Bcast(buffer, tam_poblacion, MPI_INDIVIDUO, 0, MPI_COMM_WORLD);

	for (int i=0; i<tam_poblacion; i++) {
		memcpy(poblacion[i], &buffer[i], sizeof(Individuo));
		#ifdef DEBUG
			// printArray(200, poblacion[i]->array_int);
			// printf("Received %d individuo with fitness %.0lf from %d\n", i, poblacion[i]->fitness, 0);
		#endif
	}
}

/**
 * @brief 
 * Los procesos envian sus mejores NUM_IND individuos al proceso root
 * 
 * @param poblacion 
 * @param rank 
 * @param generation
 * @param tam_poblacion
 */
void send_best_local_population(Individuo **poblacion, int rank, int generation, int tam_poblacion)
{
	// #ifdef DEBUG
	// 	 printf("Gathering best individuos from %d to %d tag: %d\n", rank, 0, generation);
	// #endif
	Individuo buffer[tam_poblacion];
	for(int i=0; i< tam_poblacion; i++) {
		buffer[i] = *(poblacion[i]);
	}

	MPI_Gather(buffer, tam_poblacion, MPI_INDIVIDUO, NULL, 0, MPI_INDIVIDUO, 0, MPI_COMM_WORLD);
}

/**
 * @brief 
 * El proceso root usa este metodo para enviar los mejores individuos a todos los procesos
 *  
 * @param poblacion 
 * @param tam_poblacion 
 */
void send_best_population(Individuo **poblacion, int tam_poblacion)
{
	Individuo buffer[tam_poblacion];
	for (int i=0; i<tam_poblacion; i++) {
		buffer[i] = *(poblacion[i]);
	}
	MPI_Bcast(buffer, tam_poblacion, MPI_INDIVIDUO, 0, MPI_COMM_WORLD);
	#ifdef DEBUG
		// printf("Sending best population to all\n");
	#endif
}

/**
 * @brief Get the best population object
 * Este metodo es usado por el proceso root para recibir los mejores individuos de los otro procesos.
 * 
 * @param population 
 * @param populationSize 
 * @param processesNumber 
 */
void get_best_population(
	Individuo **population,
	int populationSize,
	int processesNumber
) {
	int bufferSize = populationSize * processesNumber;
	Individuo buffer[bufferSize];
	for (int i=0; i<populationSize; i++) {
		buffer[i] = *(population[i]);
	}

	MPI_Gather(buffer, populationSize, MPI_INDIVIDUO, &buffer, populationSize, MPI_INDIVIDUO, 0, MPI_COMM_WORLD);

	for (int i=0; i<bufferSize; i++) {
		memcpy(population[i], &buffer[i], sizeof(Individuo));
		// #ifdef DEBUG
		// 	printArray(200, population[i]->array_int);
		// 	printf("Received %d individuo with fitness %.0lf from %d\n", i, population[i]->fitness, 0);
		// #endif
	}

}

void crear_individuo(int *individuo, int n, int m)
{
	int i=0, value;
	
	// inicializa array de elementos
	memset(individuo, -1, m);
	
	while(i < m) {
		value = aleatorio(n);
		// si el nuevo elemento no está en el array...
		if(!find_element(individuo, i, value)) {
			individuo[i] = value;  // lo incluimos
			i++;
		}
	}
}

int comp_array_int(const void *a, const void *b) {
	return (*(int *)a - *(int *)b);
}

int comp_fitness(const void *a, const void *b) {
	/* qsort pasa un puntero al elemento que está ordenando */
	return (*(Individuo **)a)->fitness - (*(Individuo **)b)->fitness;
}

/**
 * @brief 
 * El proceso root genera los individuos y los distribuyen entre los otros procesos
 * 
 * @param population 
 * @param distances 
 * @param n 
 * @param individuoSize 
 * @param populationSize 
 * @param processes 
 * @param rank 
 */
void populate_send_local_populations(
	Individuo **population,
	const double *distances, 
	int n, 
	int individuoSize, 
	int populationSize, 
	int processes,
	int rank
) {
	int missingIndividuos = populationSize % (processes);
	int localPopulationSize = populationSize / (processes);
	int rootRank = 0;
	Individuo *localPopulation; 
	Individuo *buffer;
	localPopulation = (Individuo *) malloc(sizeof(Individuo) * localPopulationSize);

	if(rank == rootRank) {
		buffer = (Individuo *) malloc(sizeof(Individuo) * populationSize);
		// crea cada individuo (array de enteros aleatorios)
		for(int i = 0; i < populationSize; i++) {
			population[i] = (Individuo *) malloc(sizeof(Individuo));
			crear_individuo(population[i]->array_int, n, individuoSize);
			// calcula el fitness del individuo
			fitness(distances, population[i], n, individuoSize);
			buffer[i] = *(population[i]);
			// #ifdef DEBUG
			// 	printArray(individuoSize, buffer[i].array_int);
			// 	printf("Adding %d individuo with %.0lf fitness to the buffer\n", i, buffer[i].fitness);
			// #endif
		}
		
		MPI_Scatter(buffer, localPopulationSize, MPI_INDIVIDUO, localPopulation, localPopulationSize, MPI_INDIVIDUO, rootRank, MPI_COMM_WORLD);
		free(buffer);

		//Si la division populationSize / processes no es entera se anaden los individuos restantes
		//al proceso root
		if(missingIndividuos > 0) {
			int initialPosition = processes * localPopulationSize;
			for (int i = 0; i < missingIndividuos; i++)
			{
				population[i + localPopulationSize] = population[i + initialPosition]; 
			}
		}
	} else {
		for (int i=0; i<localPopulationSize; i++) {
			population[i] = (Individuo *) malloc(sizeof(Individuo));
		}

		MPI_Scatter(NULL, 1, MPI_INDIVIDUO, localPopulation, localPopulationSize, MPI_INDIVIDUO, rootRank, MPI_COMM_WORLD);

		for (int i=0; i<localPopulationSize; i++) {
			memcpy(population[i], &localPopulation[i], sizeof(Individuo));
			// #ifdef DEBUG
			// 	printArray(individuoSize, population[i]->array_int);
			// 	printf("rank: %d Received individuo with fitness %.0lf from %d\n", rank, population[i]->fitness, rootRank);
			// #endif
		}
	}
	free(localPopulation);

	// ordena individuos segun la funcion de bondad (mayor "fitness" --> mas aptos)
	qsort(population, localPopulationSize, sizeof(Individuo *), comp_fitness);	
}

/**
 * @brief 
 * 
 * @param d 
 * @param n tamano del conjunto A
 * @param m tamano de cada subconjunto B
 * @param n_gen numero de generaciones
 * @param tam_pob tamano de la poblacion de individuos.
 * @param sol 
 * @return double 
 */
double aplicar_mh(const double *d, int n, int m, int n_gen, int tam_pob, int *sol)
{
	int i, g, mutation_start, p, my_rank;
	MPI_Status status;
	double value;

	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	
	Individuo **poblacion;
	int totalIndividuos = tam_pob / p;
	int numInd = NUM_IND > totalIndividuos ? totalIndividuos : NUM_IND;
	int missingIndividuos = tam_pob % p;
	#ifdef DEBUG
		// printf("Total de individuos: %d\n", totalIndividuos);
	#endif

	// crea poblacion inicial (array de individuos)
	poblacion = (Individuo **) malloc(tam_pob * sizeof(Individuo *));
	assert(poblacion);
	populate_send_local_populations(poblacion, d, n, m, tam_pob, p, my_rank);
	
	// Los otros process calcularan la mejor heuristica en sus islas

	if(my_rank == 0) {
		totalIndividuos += missingIndividuos;
	}

	// Cada proceso recibe su poblacion local
	// evoluciona la poblacion durante un numero de generaciones
	for(g = 0; g < n_gen; g++)
	{
		// los hijos de los ascendientes mas aptos sustituyen a la ultima mitad de los individuos menos aptos
		for(i = 0; i < (totalIndividuos/2) - 1; i += 2) {
			cruzar(poblacion[i], poblacion[i+1], poblacion[totalIndividuos/2 + i], poblacion[totalIndividuos/2 + i + 1], n, m);
		}
		
		// inicia la mutacion a partir de 1/4 de la poblacion
		mutation_start = totalIndividuos/4;
		
		// muta 3/4 partes de la poblacion
		for(i = mutation_start; i < totalIndividuos; i++) {
			mutar(poblacion[i], n, m);
		}
		
		// recalcula el fitness del individuo
		for(i = 0; i < totalIndividuos; i++) {
			fitness(d, poblacion[i], n, m);
		}
		
		// ordena individuos segun la funcion de bondad (mayor "fitness" --> mas aptos)
		qsort(poblacion, totalIndividuos, sizeof(Individuo *), comp_fitness);
		
		if (PRINT) {
			printf("Generacion %d - p:%d - ", g, my_rank);
			printf("Fitness = %.0lf\n", -(poblacion[0]->fitness));
		}

		if (NUM_GEN - 1 != (g % NUM_GEN)) {
			continue;
		}

		if(my_rank > 0) {
			send_best_local_population(poblacion, my_rank, g/NUM_GEN, numInd);
			get_local_population(poblacion, numInd, my_rank);
		} else {
			get_best_population(poblacion, numInd, p);
			qsort(poblacion, tam_pob, sizeof(Individuo *), comp_fitness);
			send_best_population(poblacion, numInd);
		}
	}

	if(my_rank == 0) {
		// Este es el caso de que el n_gen % NUM_GEM no es 0 y no se quiere perder las ultimas generaciones.
		if(n_gen % NUM_GEN > 0) {
			get_best_population(poblacion, numInd, p);
			qsort(poblacion, tam_pob, sizeof(Individuo *), comp_fitness);
			//printf("process is getting extra generation the final solution\n");
		}

		qsort(poblacion[0]->array_int, m, sizeof(int), comp_array_int);
		memmove(sol, poblacion[0]->array_int, m*sizeof(int));
		
		// almacena el mejor valor obtenido para el fitness
		value = -(poblacion[0]->fitness);
	} else {
		if(n_gen % NUM_GEN > 0) { 
			send_best_local_population(poblacion, my_rank, NUM_GEN + 1, numInd);
		}
	}
	
	// se libera la memoria reservada
	free(poblacion);
	
	// devuelve el valor obtenido para el fitness
	return value;
}

void cruzar(Individuo *padre1, Individuo *padre2, Individuo *hijo1, Individuo *hijo2, int n, int m)
{
	// Elegir un "punto" de corte aleatorio a partir del que se realiza el intercambio de los genes
	int pointCut = aleatorio(m);
	// Los primeros genes del padre1 van al hijo1. Idem para el padre2 e hijo2.
	for(int i = 0; i < pointCut; i++) {
		hijo1->array_int[i] = padre1->array_int[i];
		hijo2->array_int[i] = padre2->array_int[i];
	}
	
	// Y los restantes son del otro padre, respectivamente.
	for(int i = pointCut; i < m; i++) {
		// Factibilizar: eliminar posibles repetidos de ambos hijos
		// Si encuentro alguno repetido en el hijo1, lo cambio por otro que no este en el conjunto
		int parentElement = padre2->array_int[i];
		while(find_element(hijo1->array_int, m, parentElement)) {
			parentElement = aleatorio(n);
		}
		hijo1->array_int[i] = parentElement;
	}

	// Y los restantes son del otro padre, respectivamente.
	for(int i = pointCut; i < m; i++) {
		// Factibilizar: eliminar posibles repetidos de ambos hijos
		// Si encuentro alguno repetido en el hijo1, lo cambio por otro que no este en el conjunto
		int parentElement = padre1->array_int[i];
		while(find_element(hijo2->array_int, m, parentElement)) {
			parentElement = aleatorio(n);
		}
		hijo2->array_int[i] = parentElement;
	}
}

double distancia_ij(const double *d, int i, int j, int n)
{
	// I commented this line the distance from i to j should be the value of the set instead of 0
	//double dist = 0.0;
	
	// Devuelve la distancia entre dos elementos de la matriz 'd'
	return d[(i * n) + j];
}

// Determina la calidad del individuo calculando la suma de la distancia entre cada par de enteros
void fitness(const double *d, Individuo *individuo, int n, int m)
{
	double fitness = 0.0;
	for(int i = 0; i < m; i++) {
		for(int j= 0; j < m; j++) {
			if(individuo->array_int[i] < individuo->array_int[j]) {
				fitness += distancia_ij(d, individuo->array_int[i], individuo->array_int[j], n);
			}
		}
	}

	individuo->fitness = fitness;
}
	
/**
 * @brief 
 * 
 * @param actual 
 * @param n size of set A 
 * @param m 
 */
void mutar(Individuo *actual, int n, int m)
{
	// Decidir cuantos elementos mutar:
	// Si el valor es demasiado pequeño la convergencia es muy pequeña y si es demasiado alto diverge
	double mutationRate = getDynamicMutationRate(m);
	
	for (int i = 0; i < m; i++)
	{
		if (mutationRate < random_float(0,1)) {
			continue;
		}
		// Cambia el valor de algunos elementos de array_int de forma aleatoria
		int newElement = aleatorio(n);
		// teniendo en cuenta que no puede haber elementos repetidos.
		while(find_element(actual->array_int, m, newElement)) {
			// Cambia el valor de algunos elementos de array_int de forma aleatoria
			newElement = aleatorio(n);
		}

		actual->array_int[i] = newElement;
	}
}

/**
 * 
 * This function will return the total of mutations to do using the size
 * @param int size with the array size
 * @return double with the rate
 * 
 * */
double getDynamicMutationRate(int size)
{
	return size * MUTATION_RATE / 1000;
}
