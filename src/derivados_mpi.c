#include <mpi.h>
#include "../include/mdp.h"
/**
 *  Crea un nuevo tipo de datos derivado en MPI
 *  Necesario para el envio/recepcion de mensajes con datos de tipo Individuo
 **/



void crear_tipo_datos(int m)
{
	int blocklen[2] = {m, 1};
	MPI_Datatype dtype[2] = { MPI_INT, MPI_DOUBLE };
	int mpi_individuo;
	
	MPI_Aint disp[2];
	disp[0] = offsetof(Individuo, array_int);
	disp[1] = offsetof(Individuo, fitness);
	
	MPI_Type_create_struct(2, blocklen, disp, dtype, &MPI_INDIVIDUO); 
	MPI_Type_commit(&MPI_INDIVIDUO);
	
	//return mpi_individuo;
}