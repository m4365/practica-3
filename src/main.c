#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>

#include "../include/io.h"
#include "derivados_mpi.c"

extern double aplicar_mh(const double *, int, int, int, int, int *);

static double mseconds() {
	struct timeval t;
	gettimeofday(&t, NULL);
	return t.tv_sec*1000 + t.tv_usec/1000;
}

int main(int argc, char **argv)
{
	MPI_Status status;

	int my_rank, p;
	int *sol;
	double *d;
//	Check Number of Input Args
	if(argc < 4) {
		fprintf(stderr,"Ayuda:\n"); 
		fprintf(stderr,"  ./programa n m nGen tamPob\n");
		return(EXIT_FAILURE);
	}
	
	int n = atoi(argv[1]);
	int m = atoi(argv[2]);
	int n_gen = atoi(argv[3]);
	int tam_pob = atoi(argv[4]);
	
//	Check that 'm' is less than 'n'
	assert(m < n);

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);

	crear_tipo_datos(m);
	
//	Generate matrix D with distance values among elements
	if (my_rank == 0) {
		d = generar_matriz_distancias(n);
	} else {
		d = (double *) malloc(n * n *sizeof(double));
	}

	//	Allocate memory for output data
	sol = (int *) malloc(m * sizeof(int));

	MPI_Bcast(d, n * n, MPI_DOUBLE, 0, MPI_COMM_WORLD); 
	#ifdef DEBUG
		// printf("Matriz B recibida por proceso %d\n", my_rank);
	#endif

	#ifdef TIME
		double t1 = mseconds();
	#endif
	
//	Call Metaheuristic
	double value = aplicar_mh(d, n, m, n_gen, tam_pob, sol);
	
	#ifdef TIME
		double tt = 0.0;
		double t2 = mseconds();
		double time = t2 - t1;
		MPI_Reduce(&time, &tt, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	#endif

	if (my_rank == 0) {
		#ifdef TIME
			printf("Time = %.6lf seconds\n", tt);
		#endif

		#ifdef DEBUG
			print_solution(n, m, sol, value);
		#endif
	}

	//	Free Allocated Memory
	free(d);

	//	Free Allocated Memory	
	free(sol);
		
	MPI_Finalize();
	return(EXIT_SUCCESS);
}
